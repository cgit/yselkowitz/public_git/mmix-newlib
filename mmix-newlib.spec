%global __strip mmix-strip
%global __objdump mmix-objdump
%global _binaries_in_noarch_packages_terminate_build 0

Name:           mmix-newlib
Version:        2.2.0.20150623
Release:        1%{?dist}
Summary:        Cygwin cross-compiler runtime

License:        GPLv2 with exceptions
Group:          Development/Libraries
URL:            http://sourceware.org/newlib/
Source0:        ftp://sourceware.org/pub/newlib/newlib-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mmix-binutils
BuildRequires:  mmix-gcc
BuildRequires:  texinfo

%description
Newlib is a C library intended for use on embedded systems. It is a
conglomeration of several library parts, all under free software licenses
that make them easily usable on embedded products.


%prep
%setup -q -n newlib-%{version}


%build
mkdir -p build
pushd build

`pwd`/../configure \
  --prefix=%{_prefix} \
  --build=%_build --host=%_host \
  --target=mmix \
  --enable-multilib

make %{?_smp_mflags} all
popd


%install
pushd build
make DESTDIR=$RPM_BUILD_ROOT install tooldir=%{_prefix}/mmix
popd

mv $RPM_BUILD_ROOT%{_prefix}/mmix/{,sys-}include

# these are provided by other packages
rm -fr $RPM_BUILD_ROOT%{_datadir}/info


%files
%doc COPYING.NEWLIB newlib/README
%dir %{_prefix}/mmix
%dir %{_prefix}/mmix/lib
%{_prefix}/mmix/lib/crt0.o
%{_prefix}/mmix/lib/libc.a
%{_prefix}/mmix/lib/libg.a
%{_prefix}/mmix/lib/libm.a
%{_prefix}/mmix/lib/libnosys.a
%{_prefix}/mmix/lib/nosys.specs
%dir %{_prefix}/mmix/lib/gnuabi
%{_prefix}/mmix/lib/gnuabi/crt0.o
%{_prefix}/mmix/lib/gnuabi/libc.a
%{_prefix}/mmix/lib/gnuabi/libg.a
%{_prefix}/mmix/lib/gnuabi/libm.a
%{_prefix}/mmix/lib/gnuabi/libnosys.a
%{_prefix}/mmix/lib/gnuabi/nosys.specs
%{_prefix}/mmix/sys-include/


%changelog
* Wed Aug 19 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 2.2.0.20150623-1
- Initial RPM release.
